#include <iostream>

void test_mod(int &v1, int v2) // & - reference
{
	v1 += 5; // will modify passed variable
	v2 += 5;
}

int main()
{
	int var1 = 0;
	int var2 = 0;
	test_mod(var1, var2);

	std::cout << var1 << '\n'; // 5
	std::cout << var2 << '\n'; // 0

	return 0;
}