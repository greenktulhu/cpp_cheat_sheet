#include <iostream>
#include <string>

int main()
{
	int vi = 11;
	std::string vs = "vs";
	
	std::cout << "cout " << vi << ' ' << vs << '\n';

	printf("printf %i %s\n", vi, vs.c_str());

	return 0;
}