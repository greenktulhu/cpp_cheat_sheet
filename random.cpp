#include <iostream>
#include <random>
#include <chrono>
// #include <ctime>

int main()
{
	unsigned seed1 = std::chrono::system_clock::now().time_since_epoch().count();

	std::default_random_engine randomGenerator(seed1); // time(0)
	std::uniform_int_distribution<int> randInt(1, 5);

	std::cout << randInt(randomGenerator) << '\n';
	std::cout << randInt(randomGenerator) << '\n';

	std::uniform_real_distribution<float> randFloat(0.0f, 1.0f);

	std::cout << randFloat(randomGenerator) << '\n';
	std::cout << randFloat(randomGenerator) << '\n';


	return 0;
}