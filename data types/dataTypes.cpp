#include <iostream>

int main()
{
	// integers
	int i1 = 2147483647; // same as signed int	4bytes	-2147483648 to 2147483647
	unsigned int i2 = 4294967295l; // 4bytes	0 to 4294967295
	short int i3 = 32767; // 2bytes	-32768 to 32767
	unsigned short int i4 = 65535; // 2bytes	0 to 65,535
	long int i5 = 2147483647; // 8bytes	-2,147,483,648 to 2,147,483,647
	unsigned long int i6 = 4294967295; // 8bytes	0 to 4,294,967,295
	long long int iv7 = 9223372036854775807; // 8bytes	-(2^63) to (2^63)-1
	// unsigned long long int i8 = 18446744073709551615; // 8bytes	0 to 18,446,744,073,709,551,615

	// floats TODO: maximum values.
	float f1 = 9999.99; // 4bytes
	double f2 = 9999.99; // 8bytes
	long double f3 = 9999.99; // 12bytes

	// characters
	char c1 = 'a'; // 1byte	-127 to 127 or 0 to 255
	unsigned char c2 = 'a'; // 1byte	0 to 255

	//boolean
	bool b1 = true;

	std::cout << b1 << '\n';
}