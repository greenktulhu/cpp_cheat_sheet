#include <iostream>

class Human
{
public:
	Human(std::string name, int level) // constructor definition
{
	_name = name;
	_level = level;
}

	void growl()
	{
		std::cout << "Growl!!!\n";
	}
	void aaaa();  // metod declaration

	void setName(std::string name)
	{
		_name = name;
	}

	std::string getName()
	{
		return _name;
	}
private:
	std::string _name;
	int _level;

};

void Human::aaaa()  // metod definition
{
	std::cout << "AAAA!!!\n";
}

// Human::Human(std::string name, int level) // constructor definition
// {
// 	_name = name;
// 	_level = level;
// }

int main()
{
	Human human1("Ryan", 1);
	human1.growl();
	human1.aaaa();

	std::cout << human1.getName() << '\n';

	return 0;
}