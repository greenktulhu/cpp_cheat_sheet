#include <iostream>

struct C
{
	int a;
	float b;
};

struct D
{
	int a;
	float b;
	C ce;
};

int main()
{
	C ce = {1, 3.14};

	D de = {2, 2.2, {5, 6.8}};

	std::cout << de.ce.b << '\n';
	std::cout << de.a << '\n';

	return 0;
}