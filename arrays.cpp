#include <iostream>
#include <string>

void print_array(std::string array[], int size)
{
	for (int i = 0; i < size; i++)
	{
		std::cout << array[i] << '\n';
	}
}

int main()
{
	const int numItems = 4;
	std::string itemsNames[numItems] = {"name1", "ItemName", "someItem", "Qwerty"};
	int itemsAmount[numItems] = {6, 0, 0, 1};

	int multidimensionalArray[2][3] = {{1, 2, 3}, {4, 5, 6}}; // multidimensional array

	print_array(itemsNames, numItems);

	return 0;
}