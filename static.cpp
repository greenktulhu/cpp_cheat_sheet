#include <iostream>

void static_test()
{
	static int var = 1; // var keeps in the next function call after ending of previous function call
	std::cout << var << '\n';
	var++;
}

int main()
{
	static_test();
	static_test();
	static_test();
	static_test();
	static_test();
	static_test();
	return 0;
}